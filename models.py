from typing import List, Optional, Any
from enum import IntEnum

class GameState(IntEnum):
	Waiting = 0
	Running = 1
	Finished = 2
	Cancelled = 3
	Forfeited = 4

class PieceType(IntEnum):
	Pawn = 1
	Rook = 2
	Knight = 3
	Bishop = 4
	Queen = 5
	King = 6

class PieceState(IntEnum):
	Alive = 1
	Captured = 2

class MoveType(IntEnum):
	Regular = 1
	DoublePush = 2
	EnPassant = 3
	Castle = 4

class Position:
	__slots__ = ('x', 'y')
	
	x: int
	y: int
	
	def __init__(self, x: int, y: int) -> None:
		self.x = x
		self.y = y
	
	def __hash__(self) -> int:
		return self.x * 10 + self.y
	
	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Position):
			return False
		return (self.x, self.y) == (other.x, other.y)
	
	def __add__(self, other: 'Position') -> 'Position':
		return Position(self.x + other.x, self.y + other.y)
	
	def __str__(self) -> str:
		return '<{}, {}>'.format(self.x, self.y)
	
	def __repr__(self) -> str:
		return str(self)

class Game:
	__slots__ = ('id', 'players', 'private', 'state', 'ply', 'pieces')
	
	id: str
	players: List[Any]
	private: bool
	state: GameState
	ply: int
	pieces: List['Piece']
	
	def __init__(self, id: str, *, private: bool) -> None:
		self.id = id
		self.players = []
		self.private = private
		self.state = GameState.Waiting
		self.ply = 0
		self.pieces = []

class Piece:
	__slots__ = ('id', 'player', 'type', 'pos', 'state', 'has_moved')
	
	id: int
	player: int
	type: PieceType
	pos: Position
	state: PieceState
	has_moved: bool
	
	def __init__(self, id: int, player: int, type: PieceType, pos: Position) -> None:
		self.id = id
		self.player = player
		self.type = type
		self.pos = pos
		self.state = PieceState.Alive
		self.has_moved = False
	
	def __str__(self):
		return "{} #{}".format(self.get_type_display(), self.id)

class Move:
	__slots__ = ('ply', 'type', 'piece', 'captured', 'pos')
	
	ply: int
	type: MoveType
	piece: Piece
	captured: Optional[Piece]
	pos: Position
	
	def __init__(self, ply: int, type: MoveType, piece: Piece, pos: Position, *, captured: Optional[Piece] = None) -> None:
		self.ply = ply
		self.type = type
		self.piece = piece
		self.pos = pos
		self.captured = captured
