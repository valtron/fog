from python:3.7

EXPOSE 8000
WORKDIR /usr/src/app
COPY . .

RUN apt-get update && apt-get install -y \
	--no-install-recommends && rm -rf /var/lib/apt/lists/* && \
	pip install -r requirements.txt

CMD ["python", ".", "0.0.0.0", "8000"]
