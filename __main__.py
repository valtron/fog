import sys
import uuid
import json
import asyncio
from typing import Dict

import jinja2
from aiohttp import web, WSMsgType

from models import Game, GameState
import logic

def main(host: str, port: str) -> None:
	loop = asyncio.get_event_loop()
	loop.create_task(_windows_ctrl_c_workaround())
	app = create_app(loop)
	web.run_app(app, host = host, port = int(port))

def create_app(loop: asyncio.AbstractEventLoop) -> web.Application:
	app = web.Application(loop = loop)
	app['jinja'] = jinja2.Environment(
		loader = jinja2.FileSystemLoader('tmpl'),
		autoescape = jinja2.select_autoescape(default = True),
	)
	app['games'] = {}
	
	app.router.add_get('/', page_home)
	app.router.add_get('/ws/game', page_websocket)
	app.router.add_get('/ws/game/{gameid}', page_websocket)
	app.router.add_static('/static', path = 'static', name = 'static')
	return app

async def page_home(req: web.Request) -> web.Response:
	tmpl = req.app['jinja'].get_template('home.html')
	return web.Response(
		status = 200, content_type = 'text/html',
		text = tmpl.render(title = "Fog Chess"),
	)

async def page_websocket(req: web.Request) -> web.Response:
	gameid = req.match_info.get('gameid') or ''
	
	ws = web.WebSocketResponse()
	await ws.prepare(req)
	
	games = req.app['games']
	
	game = _find_game(req.app['games'], gameid)
	assert game.state is GameState.Waiting
	player_num = len(game.players)
	assert player_num < 2
	
	await logic.game_join(game, ws)
	
	async for msg in ws:
		type = msg.type
		data = msg.data
		if type == WSMsgType.ERROR:
			print("ws connection closed with exception {}".format(ws.exception()))
			continue
		if type == WSMsgType.TEXT:
			data = json.loads(data)
			running = await logic.game_move(game, player_num, data['piece'], data['x'], data['y'])
			if not running:
				break
			continue
		print("ws unknown", type, data[:30])
	
	games.pop(game.id, None)
	await logic.game_exit(game, ws)
	
	return ws

def _find_game(games: Dict[str, Game], gameid: str) -> Game:
	if gameid:
		if gameid not in games:
			games[gameid] = Game(gameid, private = True)
		return games[gameid]
	
	# Find a public game
	found_game = None
	for game in games.values():
		if game.private or game.state is not GameState.Waiting:
			continue
		found_game = game
		break
	
	if found_game is None:
		gameid = str(uuid.uuid4())
		found_game = Game(gameid, private = False)
		games[gameid] = found_game
	
	return found_game

async def _windows_ctrl_c_workaround() -> None:
	import os
	if os.name != 'nt': return
	# https://bugs.python.org/issue23057
	while True:
		await asyncio.sleep(0.1)

if __name__ == '__main__':
	main(*sys.argv[1:])
