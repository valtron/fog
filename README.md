Fog
===

Chess with fog of war. The player can only see squares that his pieces can move to.

TODO
----

- [ ] En passant
- [ ] Castling
- [ ] Move validation
- [ ] Promotion choice
