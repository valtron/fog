class FogGame {
	constructor($form, $game) {
		$form.addEventListener('submit', this._onFormSubmit.bind(this));
		this.$game = $game;
		this.gamestate = null;
		this.ws = null;
		this._selected_piece_id = null;
		this._hovered_piece_id = null;
		this._createBoard();
	}
	_onFormSubmit(evt) {
		const $input = evt.target.gameid;
		const gameid = $input.value;
		$input.value = '';
		const loc = window.location;
		const isHTTPS = (loc.protocol == 'https:');
		let wsurl = 'ws' + (isHTTPS ? 's' : '') + '://' + loc.host + '/ws/game';
		if (gameid) {
			wsurl += '/' + encodeURIComponent(gameid);
		}
		
		if (this.ws) {
			this.ws.onmessage = null;
			this.ws.close();
		}
		this.ws = new WebSocket(wsurl);
		this.ws.onmessage = this._onWSMessage.bind(this);
		this._showMessage("Matchmaking...");
		
		evt.preventDefault();
		return false;
	}
	_onWSMessage(evt) {
		this.refresh(JSON.parse(evt.data));
	}
	_createBoard() {
		const $board = this.$game.querySelector('.board');
		for (let row = 7; row >= 0; --row) {
			for (let col = 0; col < 8; ++col) {
				const $cell = document.createElement('div');
				$cell.classList.add('cell', 'cell-' + col + '_' + row, 'cell-' + ((col+row)%2));
				$cell.dataset.x = col;
				$cell.dataset.y = row;
				$cell.addEventListener('mouseover', () => this._hoverCellIn($cell));
				$cell.addEventListener('mouseout', () => this._hoverCellOut($cell));
				$cell.addEventListener('click', () => this._cellClick($cell));
				$board.appendChild($cell);
			}
		}
	}
	refresh(data) {
		if (data.error) {
			alert(data.message);
			return;
		}
		
		this.gamestate = data;
		
		for (const $cell of this.$game.querySelectorAll('.board .cell')) {
			$cell.classList.remove('p0', 'p1');
			$cell.classList.add('hidden');
			$cell.textContent = '';
		}
		
		const pieces = data.pieces;
		const $captured = this.$game.querySelector('.captured');
		while ($captured.hasChildNodes()) {
			$captured.removeChild($captured.lastChild);
		}
		
		for (const piece of Object.values(pieces)) {
			if (piece.state != 2) continue;
			const $elm = document.createElement('li');
			$elm.classList.add('p' + piece.player);
			$elm.textContent = PIECECHAR[piece.type];
			$captured.appendChild($elm);
		}
		
		for (const square of data.squares) {
			const $cell = this._cell(square.pos);
			$cell.classList.remove('hidden');
			if (square.piece == null) {
				delete $cell.dataset.piece_id;
			} else {
				$cell.dataset.piece_id = square.piece;
				const piece = pieces[square.piece];
				$cell.textContent = PIECECHAR[piece.type];
				$cell.classList.add('p' + piece.player);
				if (piece.player == data.me && data.my_turn && data.valid_moves[square.piece]) {
					$cell.classList.add('movable');
				}
			}
		}
		
		this.$game.classList.remove('my-turn');
		if (data.state == 0) {
			this._showMessage("Matchmaking...");
		} else if (data.state == 1) {
			if (data.my_turn) {
				this._showMessage("Your turn");
				this.$game.classList.add('my-turn');
				document.title = "(!) Fog Chess";
			} else {
				this._showMessage("Waiting for other player...");
				document.title = "Fog Chess";
			}
		} else if (data.state == 2) {
			this._showMessage("Game Over");
		} else if (data.state == 3) {
			this._showMessage("Game cancelled");
		} else if (data.state == 4) {
			this._showMessage("Game forfeited");
		}
	}
	_cell(pos) {
		return this.$game.querySelector('.board .cell.cell-' + pos[0] + '_' + pos[1]);
	}
	_hoverCellIn($cell) {
		this._hovered_piece_id = $cell.dataset.piece_id;
		this._updateBoard();
	}
	_hoverCellOut($cell) {
		this._hovered_piece_id = null;
		this._updateBoard();
	}
	_cellPiece($cell) {
		const piece_id = $cell.dataset.piece_id;
		if (piece_id == null) return null;
		return this.gamestate.pieces[piece_id];
	}
	_cellClick($cell) {
		const gamestate = this.gamestate;
		if (gamestate == null) return;
		if (!gamestate.my_turn) return;
		const piece = this._cellPiece($cell);
		
		if (this._selected_piece_id != null) {
			if ($cell.classList.contains('move-option')) {
				this.ws.send(JSON.stringify({
					piece: this._selected_piece_id,
					x: parseInt($cell.dataset.x),
					y: parseInt($cell.dataset.y)
				}));
				this._selected_piece_id = null;
			} else if (piece) {
				if (piece.player == gamestate.me) {
					this._selected_piece_id = piece.id;
				} else {
					this._selected_piece_id = null;
				}
			} else {
				this._selected_piece_id = null;
			}
		} else if (piece && piece.player == gamestate.me) {
			this._selected_piece_id = piece.id;
		} else {
			this._selected_piece_id = null;
		}
		
		this._updateBoard();
	}
	_updateBoard() {
		for (const $cell of this.$game.querySelectorAll('.board .cell')) {
			$cell.classList.remove('move-option', 'possible-move');
		}
		
		if (this._hovered_piece_id != null) {
			this._highlightMoves(this._hovered_piece_id, 'possible-move');
		}
		
		if (this._selected_piece_id != null) {
			this._highlightMoves(this._selected_piece_id, 'move-option');
		}
	}
	_highlightMoves(piece_id, cls) {
		if (!this.gamestate || piece_id == null) return;
		
		const moves = this.gamestate.valid_moves[piece_id];
		if (!moves) return;
		
		for (const pos of moves) {
			this._cell(pos).classList.add(cls);
		}
	}
	_turn() {
		if (!this.gamestate) return null;
		return this.gamestate.turn;
	}
	_showMessage(text) {
		this.$game.querySelector('.message').textContent = text;
	}
}

const PIECECHAR = {
	King  : String.fromCharCode(0x265A),
	Queen : String.fromCharCode(0x265B),
	Rook  : String.fromCharCode(0x265C),
	Bishop: String.fromCharCode(0x265D),
	Knight: String.fromCharCode(0x265E),
	Pawn  : String.fromCharCode(0x265F)
};
