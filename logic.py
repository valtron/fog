from typing import Dict, List, Tuple, Any, Iterable
import json

from models import Game, GameState, PieceType, PieceState, Position, Piece, Move, MoveType

Board = Dict[Position, Piece]

async def game_join(game: Game, ws: Any) -> None:
	game.players.append(ws)
	if len(game.players) != 2:
		return
	game.state = GameState.Running
	_game_init_player(game, 0)
	_game_init_player(game, 1)
	await _send_game_state(game)

async def game_exit(game: Game, ws: Any) -> None:
	if game.state is GameState.Waiting:
		game.state = GameState.Cancelled
	elif game.state is GameState.Running:
		game.state = GameState.Forfeited
		for player_num, ws_other in enumerate(game.players):
			if ws_other is ws: continue
			state = _game_state(game, player_num)
			await ws_other.send_str(json.dumps(state))

def _game_state(game: Game, player_num: int) -> Dict[str, Any]:
	pieces: Dict[int, Dict[str, Any]] = {}
	squares: List[Dict[str, Any]] = []
	valid_moves: Dict[int, List[Tuple[int, int]]] = {}
	board: Board = {}
	
	for piece in game.pieces:
		pieces[piece.id] = {
			'id': piece.id, 'player': piece.player,
			'type': piece.type.name, 'state': int(piece.state)
		}
		
		if piece.state is PieceState.Alive:
			pos = piece.pos
			board[pos] = piece
			
			if piece.player == player_num:
				squares.append({ 'pos': (pos.x, pos.y), 'piece': piece.id })
	
	if game.state is GameState.Running:
		my_turn = (game.ply % 2 == player_num)
		
		R = lambda p: _refl(p, player_num == 0)
		
		for piece in game.pieces:
			if piece.state is not PieceState.Alive:
				continue
			if piece.player != player_num:
				continue
			
			piece_moves = _get_valid_moves(game, board, player_num, R, piece)
			
			if not piece_moves:
				continue
			
			for pos in piece_moves:
				piece_on_pos = board.get(pos)
				piece_on_pos_id = piece_on_pos.id if piece_on_pos else None
				squares.append({ 'pos': (pos.x, pos.y), 'piece': piece_on_pos_id })
			
			if my_turn:
				valid_moves[piece.id] = [(pos.x, pos.y) for pos in piece_moves]
	else:
		my_turn = False
		for x in range(8):
			for y in range(8):
				pos = Position(x, y)
				piece_on_pos = board.get(pos)
				piece_on_pos_id = piece_on_pos.id if piece_on_pos else None
				squares.append({ 'pos': (pos.x, pos.y), 'piece': piece_on_pos_id })
	
	return {
		# Global
		'state': game.state,
		'ply': game.ply,
		'pieces': pieces,
		
		# Player specific
		'me': player_num,
		'my_turn': my_turn,
		'squares': squares,
		'valid_moves': valid_moves,
	}

async def game_move(game: Game, player_num: int, piece_id: int, pos_x: int, pos_y: int) -> bool:
	player = game.players[player_num]
	
	if game.state is not GameState.Running:
		await player.send_str(json.dumps({
			'error': 'state',
			'message': "Game isn't running.",
		}))
		return False
	
	if game.ply % 2 != player_num:
		await player.send_str(json.dumps({
			'error': 'state',
			'message': "It's not your turn.",
		}))
		return True
	
	# TODO: Validate the move
	
	pos = Position(pos_x, pos_y)
	piece_on_pos = None
	for piece in game.pieces:
		if piece.state is not PieceState.Alive: continue
		if piece.pos != pos: continue
		piece_on_pos = piece
		break
	
	piece = game.pieces[piece_id]
	promotion = _should_promote(player_num, piece, pos)
	
	if promotion:
		# TODO: Let player choose what to promote to
		piece.type = PieceType.Queen
	
	if piece_on_pos:
		if piece.player == piece_on_pos.player:
			# Must be castle
			assert piece.type is PieceType.King
			assert piece_on_pos.type is PieceType.Rook
			assert not promotion
			
			piece_on_pos.pos = piece.pos
		else:
			# Must be capture
			captured = piece_on_pos
			captured.state = PieceState.Captured
			
			if captured.type is PieceType.King:
				game.state = GameState.Finished
	else:
		if piece.type is PieceType.Pawn and abs(piece.pos.y - pos.y) == 2:
			move_type = MoveType.DoublePush
		else:
			move_type = MoveType.Regular
	
	piece.pos = pos
	piece.has_moved = True
	game.ply += 1
	
	await _send_game_state(game)
	
	return game.state is GameState.Running

async def _send_game_state(game: Game) -> None:
	for player_num, player in enumerate(game.players):
		# TODO: Confusing, this isn't the same as `Game.state`
		state = _game_state(game, player_num)
		await player.send_str(json.dumps(state))

def _should_promote(player_num: int, piece: Piece, pos: Position):
	if piece.type is not PieceType.Pawn:
		return False
	
	if pos.y == 0 and player_num == 1:
		return True
	
	if pos.y == 7 and player_num == 0:
		return True
	
	return False

def _get_valid_moves(game: Game, board: Board, player_num: int, R: Any, piece: Piece) -> List[Position]:
	pos = R(piece.pos)
	type = piece.type
	
	if type is PieceType.Pawn:
		moves = list(_pawn_moves(board, pos, R, player_num))
	else:
		moves = list(_non_pawn_moves(board, pos, R, player_num, type))
	
	if type is PieceType.King:
		if not piece.has_moved:
			assert pos.y == 0
			
			for p in game.pieces:
				if p.player != player_num: continue
				if p.type is not PieceType.Rook: continue
				if p.state is not PieceState.Alive: continue
				if p.has_moved: continue
				assert R(p.pos).y == pos.y
				if not _is_path_clear(board, piece, p):
					continue
				moves.append(p.pos)
	
	return [
		pos for pos in moves
		if 0 <= pos.x < 8 and 0 <= pos.y < 8
	]

def _is_path_clear(board: Board, king: Piece, rook: Piece) -> bool:
	king_pos = king.pos
	rook_pos = rook.pos
	
	assert king_pos.y == rook_pos.y
	
	y = king_pos.y
	xmin = min(king_pos.x, rook_pos.x)
	xmax = max(king_pos.x, rook_pos.x)
	
	for x in range(xmin + 1, xmax):
		if Position(x, y) in board:
			return False
	
	return True

def _pawn_moves(board: Board, pos: Position, R: Any, player_num: int) -> Iterable[Position]:
	ahead = R(pos + Position(0, 1))
	
	if not board.get(ahead):
		yield ahead
		# TODO: Instead of checking the y, check if the pawn has previously moved
		if pos.y == 1:
			ahead = R(pos + Position(0, 2))
			if not board.get(ahead):
				yield ahead
	
	left = R(pos + Position(-1, 1))
	left_piece = board.get(left)
	if left_piece and left_piece.player != player_num:
		yield left
	
	right = R(pos + Position(1, 1))
	right_piece = board.get(right)
	if right_piece and right_piece.player != player_num:
		yield right

def _non_pawn_moves(board: Board, pos: Position, R: Any, player_num: int, type: PieceType) -> Iterable[Position]:
	for move_type in PIECE_TYPES[type]['moves']:
		for path in _get_move_type_paths(move_type, board, pos):
			for move in path:
				new_pos = R(pos + Position(move[0], move[1]))
				p = board.get(new_pos)
				
				if not p or p.player != player_num:
					yield new_pos
				if p:
					break

def _get_move_type_paths(move_type: str, board: Board, pos: Position) -> List[List[Tuple[int, int]]]:
	if move_type == 'knight':
		return [[move] for move in KNIGHT_MOVES]
	
	N = 7
	
	if move_type == 'king':
		N = 1
		bases = LINE_BASES + DIAG_BASES
	elif move_type == 'line':
		bases = LINE_BASES
	elif move_type == 'diag':
		bases = DIAG_BASES
	else:
		assert False, "invalid move type '{}'".format(move_type)
	
	return [
		[(base[0] * i, base[1] * i) for i in range(1, N + 1)]
		for base in bases
	]

def _refl(pos: Position, is_p1: bool) -> Position:
	if is_p1:
		return pos
	return Position(pos.x, 7 - pos.y)

def _game_init_player(game: Game, player_num: int) -> None:
	player = game.players[player_num]
	if player_num == 0:
		rows = (0, 1)
	else:
		rows = (7, 6)
	for type in PieceType:
		_place_pieces(game, player_num, rows, type)

def _place_pieces(game: Game, player_num: int, rows: Tuple[int, int], type: PieceType) -> None:
	type_info = PIECE_TYPES[type]
	
	cols = type_info['cols']
	row = rows[type_info['row']]
	
	for x in cols:
		game.pieces.append(Piece(len(game.pieces), player_num, type, Position(x, row)))

PIECE_TYPES: Dict[PieceType, Dict[str, Any]] = {
	PieceType.Pawn  : {
		'row': 1,
		'cols': [0, 1, 2, 3, 4, 5, 6, 7],
		'moves': [],
	},
	PieceType.Rook  : {
		'row': 0,
		'cols': [0, 7],
		'moves': ['line'],
	},
	PieceType.Knight: {
		'row': 0,
		'cols': [1, 6],
		'moves': ['knight'],
	},
	PieceType.Bishop: {
		'row': 0,
		'cols': [2, 5],
		'moves': ['diag'],
	},
	PieceType.Queen : {
		'row': 0,
		'cols': [3],
		'moves': ['line', 'diag'],
	},
	PieceType.King  : {
		'row': 0,
		'cols': [4],
		'moves': ['king'],
	},
}

LINE_BASES = [(1, 0), (-1, 0), (0, 1), (0, -1)]
DIAG_BASES = [(1, 1), (1, -1), (-1, 1), (-1, -1)]
KNIGHT_MOVES = [
	(2, 1), (2, -1), (-2, 1), (-2, -1),
	(1, 2), (-1, 2), (1, -2), (-1, -2),
]
